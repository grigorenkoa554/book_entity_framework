﻿using Book_EntityFramework.Model;
using Microsoft.EntityFrameworkCore;

namespace Book_EntityFramework.MyDbContext
{
    public class FirstDbContext : DbContext // наследуемся от класса DbContext, чтобы настроитьсоздание таблиц
    {
        public FirstDbContext()
        {
            Database.EnsureCreated(); // генерация таблиц, если они не созданы
        }


        public DbSet<Student> Students { get; set; }
        public DbSet<Course> Courses { get; set; }
        public DbSet<Chicken> Chickens { get; set; }
        public DbSet<Kitty> Kittens { get; set; }
        //------------------------------------------------------------------------------

        public DbSet<User> Users { get; set; } // указываем таблицу, которая сгенерится. 

        public DbSet<Address> Addresses { get; set; }

        public DbSet<PlaceOfWork> PlacesOfWork { get; set; }
        public DbSet<Passport> Passports { get; set; }

        public DbSet<Car> Cars { get; set; }
        public DbSet<Mehanic> Mehanics { get; set; }
        public DbSet<CarMehanic> CarMehanics { get; set; }
        public DbSet<Driver> Drivers { get; set; }
        public DbSet<License> Licenses { get; set; }
        public DbSet<Inspection> Inspections { get; set; }
        public DbSet<Racing> Racings { get; set; }
        public DbSet<RepairEvent> RepairEvents { get; set; }
        public DbSet<MedicalExamination> MedicalExaminations { get; set; }
        public DbSet<MedicalClinic> MedicalClinics { get; set; }
        public DbSet<DriverMedicalClinic> DriverMedicalClinics { get; set; }
        public DbSet<Passenger> Passengers { get; set; }
        public DbSet<InternationalPassport> InternationalPassports { get; set; }

        public DbSet<GasStation> GasStations { get; set; }
        public DbSet<CarGasStation> CarGasStations { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) // конфигурирование
        {
            var cs = @"Data Source = 'DESKTOP-IOHQV8L'; 
                Initial Catalog='DB_for_EntityFramework';
                Integrated Security=true;"; // строка подключения
            optionsBuilder.UseSqlServer(cs); // указываем строку подключения
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Student>(x => x.HasIndex(e => new { e.Login }).IsUnique());
            modelBuilder.Entity<CarMehanic>().HasKey(sc => new { sc.CarId, sc.MehanicId });
            modelBuilder.Entity<DriverMedicalClinic>().HasKey(sc => new { sc.DriverId, sc.MedicalClinicId });
            modelBuilder.Entity<CarGasStation>().HasKey(sc => new { sc.CarId, sc.GasStationId });
        }
    }
}

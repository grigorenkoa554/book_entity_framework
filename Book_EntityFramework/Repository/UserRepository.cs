﻿using Book_EntityFramework.Model;
using Book_EntityFramework.MyDbContext;
using System.Collections.Generic;
using System.Linq;

namespace Book_EntityFramework.Repository
{
    interface IUserRepository
    {
        void Add(User user);
        List<User> GetAll();
        User GetById(int id);
        void Update(User user);
        void Delete(int id);
    }

    class UserRepository : IUserRepository
    {
        public void Add(User user)
        {
            using (var dbContext = new FirstDbContext())
            {
                dbContext.Users.Add(user); 
                dbContext.SaveChanges(); 
            }
        }

        public void Delete(int id)
        {
            using (var dbContext = new FirstDbContext())
            {
                var user = dbContext.Users.First(x => x.Id == id); 
                dbContext.Users.Remove(user);
                dbContext.SaveChanges();
            }
        }

        public List<User> GetAll()
        {
            using (var dbContext = new FirstDbContext())
            {
                var users = dbContext.Users.ToList(); 
                return users;
            }
        }

        public User GetById(int id)
        {
            using (var dbContext = new FirstDbContext())
            {
                var user = dbContext.Users.First(x => x.Id == id); 
                return user;
            }
        }

        public void Update(User user)
        {
            using (var dbContext = new FirstDbContext())
            {
                var oldUser = dbContext.Users.First(x => x.Id == user.Id);
                if (oldUser != null)
                {
                    oldUser.FirstName = user.FirstName;
                    oldUser.LastName = user.LastName;
                    oldUser.Age = user.Age;
                }
                dbContext.SaveChanges();
            }
        }
    }
}

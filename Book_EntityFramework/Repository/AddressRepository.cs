﻿using Book_EntityFramework.Model;
using Book_EntityFramework.MyDbContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Book_EntityFramework.Repository
{
    interface IAddressRepository
    {
        void Add(Address user);
        List<Address> GetAll();
        Address GetById(int id);
        void Update(Address user);
        void Delete(int id);
    }
    class AddressRepository : IAddressRepository
    {
        public void Add(Address address)
        {
            using (var dbContext = new FirstDbContext())
            {
                dbContext.Addresses.Add(address);
                dbContext.SaveChanges();
            }
        }

        public void Delete(int id)
        {
            using (var dbContext = new FirstDbContext())
            {
                var address = dbContext.Addresses.First(x => x.Id == id);
                dbContext.Addresses.Remove(address);
                dbContext.SaveChanges();
            }
        }

        public List<Address> GetAll()
        {
            using (var dbContext = new FirstDbContext())
            {
                var address = dbContext.Addresses.ToList();
                return address;
            }
        }

        public Address GetById(int id)
        {
            using (var dbContext = new FirstDbContext())
            {
                var address = dbContext.Addresses.First(x => x.Id == id);
                return address;
            }
        }

        public void Update(Address address)
        {
            using (var dbContext = new FirstDbContext())
            {
                var oldAddress = dbContext.Addresses.First(x => x.Id == address.Id);
                if (oldAddress != null)
                {
                    oldAddress.City = address.City;
                    oldAddress.Street = address.Street;
                    oldAddress.UserId = address.UserId;
                }
                dbContext.SaveChanges();
            }
        }
    }
}

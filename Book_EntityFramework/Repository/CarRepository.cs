﻿using Book_EntityFramework.Model;
using Book_EntityFramework.MyDbContext;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace Book_EntityFramework.Repository
{
    interface ICarRepository<T>
    {
        void Add(Car car);
        void AddRange(List<Car> car);
        List<Car> GetAll(CarDeepGet carDeepGet);
        Car GetById(int id, CarDeepGet carDeepGet);
        void Update(Car car);
        public void UpdateDriver(Driver driver);
        public void UpdateLicense(License license);
        void Delete(int id);
        List<T> ExecuteQuery(string sql);
        int ExecuteQueryCommon(string sql);
    }
    class CarRepository : ICarRepository<Car>
    {
        public void Add(Car car)
        {
            using (var dbContext = new FirstDbContext())
            {
                dbContext.Cars.Add(car);
                dbContext.SaveChanges();
            }
        }

        public void AddRange(List<Car> cars)
        {
            using (var dbContext = new FirstDbContext())
            {
                dbContext.Cars.AddRange(cars);
                dbContext.SaveChanges();
            }
        }

        public void Delete(int id)
        {
            using (var dbContext = new FirstDbContext())
            {
                var car = dbContext.Cars.First(x => x.Id == id);
                dbContext.Cars.Remove(car);
                dbContext.SaveChanges();
            }
        }

        public void Delete(int id, CarDeepGet carDeepGet)
        {
            using (var dbContext = new FirstDbContext())
            {
                var car = dbContext.Cars.First(x => x.Id == id);
                dbContext.Cars.Remove(car);
                dbContext.SaveChanges();
            }

        }

        public List<Car> GetAll(CarDeepGet carDeepGet = CarDeepGet.None)
        {
            using (var dbContext = new FirstDbContext())
            {
                var query = dbContext.Cars.Select(x => x);
                query = GetDeepCarQuery(query, carDeepGet);

                var cars = query.ToList();
                return cars;
            }
        }

        public Car GetById(int id, CarDeepGet carDeepGet = CarDeepGet.None)
        {
            using (var dbContext = new FirstDbContext())
            {
                var query = dbContext.Cars.Select(x => x);
                query = GetDeepCarQuery(query, carDeepGet);
                var car = query.First(x => x.Id == id);
                return car;
            }
        }

        private IQueryable<Car> GetDeepCarQuery(IQueryable<Car> query, CarDeepGet carDeepGet)
        {
            if ((carDeepGet & CarDeepGet.Driver) == CarDeepGet.Driver ||
                (carDeepGet & CarDeepGet.License) == CarDeepGet.License)
            {
                if ((carDeepGet & CarDeepGet.License) == CarDeepGet.License)
                {
                    query = query.Include(x => x.Driver).ThenInclude(x => x.License);
                }
                else
                {
                    query = query.Include(x => x.Driver);
                }
            }
            if ((carDeepGet & CarDeepGet.Inspection) == CarDeepGet.Inspection)
            {
                query = query.Include(x => x.Inspection);
            }
            if ((carDeepGet & CarDeepGet.Racing) == CarDeepGet.Racing)
            {
                query = query.Include(x => x.Racings);
            }
            if ((carDeepGet & CarDeepGet.Mehanic) == CarDeepGet.Mehanic)
            {
                query = query.Include(x => x.CarMehanics)
                    .ThenInclude(x => x.Mehanic);
            }
            if ((carDeepGet & CarDeepGet.GasStation) == CarDeepGet.GasStation)
            {
                query = query.Include(x => x.CarGasStations)
                    .ThenInclude(x => x.GasStation);
            }
            if ((carDeepGet & CarDeepGet.MedicalClinic) == CarDeepGet.MedicalClinic)
            {
                query = query.Include(x => x.Driver.DriverMedicalClinics).ThenInclude(x => x.MedicalClinic);
            }
            if ((carDeepGet & CarDeepGet.RepairEvent) == CarDeepGet.RepairEvent)
            {
                query = query.Include(x => x.RepairEvents);
            }

            if ((carDeepGet & CarDeepGet.Driver) == CarDeepGet.Driver ||
               (carDeepGet & CarDeepGet.MedicalExamination) == CarDeepGet.MedicalExamination)
            {
                if ((carDeepGet & CarDeepGet.MedicalExamination) == CarDeepGet.MedicalExamination)
                {
                    query = query.Include(x => x.Driver).ThenInclude(x => x.MedicalExaminations);
                }
                else
                {
                    query = query.Include(x => x.Driver);
                }
            }
            return query;
        }

        public void Update(Car car)
        {
            using (var dbContext = new FirstDbContext())
            {
                var oldCar = dbContext.Cars.First(x => x.Id == car.Id);
                if (oldCar != null)
                {
                    oldCar.Model = car.Model;
                    oldCar.Number = car.Number;
                }
                dbContext.SaveChanges();
            }
        }

        public void UpdateDeep(Car car)
        {
            using (var dbContext = new FirstDbContext())
            {
                var oldCar = dbContext.Cars.First(x => x.Id == car.Id);
                dbContext.Cars.Update(car);
                oldCar.Driver.FirstName = "Valentin";
                dbContext.Entry(oldCar).State = EntityState.Modified;
                dbContext.Update(oldCar);
                dbContext.SaveChanges();
            }
        }

        public void UpdateDriver(Driver driver)
        {
            using (var dbContext = new FirstDbContext())
            {
                var old = dbContext.Drivers.First(x => x.Id == driver.Id);
                if (old != null)
                {
                    old.FirstName = driver.FirstName;
                    old.LastName = driver.LastName;
                }
                dbContext.SaveChanges();
            }
        }

        public void UpdateLicense(License license)
        {
            using (var dbContext = new FirstDbContext())
            {
                var old = dbContext.Licenses.First(x => x.Id == license.Id);
                if (old != null)
                {
                    old.Text = license.Text;
                }
                dbContext.SaveChanges();
            }
        }

        public List<Car> ExecuteQuery(string sql)
        {
            using (var dbContext = new FirstDbContext())
            {
                // "SELECT Top 3 * FROM dbo.Car where Id > 3"
                return dbContext.Cars.FromSqlRaw(sql).ToList();
            }
        }

        public int ExecuteQueryCommon(string sql)
        {
            using (var dbContext = new FirstDbContext())
            {
                // "SELECT Top 3 * FROM dbo.Car where Id > 3"
                return dbContext.Database.ExecuteSqlCommand(sql);
            }
        }
    }

    public enum CarDeepGet
    {
        None = 1,
        Driver = 2,
        Inspection = 4,
        License = 8,
        DriverLicense = 10, // Driver + License
        Racing = 16,
        Mehanic = 32,
        RepairEvent = 64,
        MedicalExamination = 128,
        MedicalClinic = 256,
        GasStation = 512
    }
}
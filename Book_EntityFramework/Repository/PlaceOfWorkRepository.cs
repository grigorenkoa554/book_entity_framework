﻿using Book_EntityFramework.Model;
using Book_EntityFramework.MyDbContext;
using System.Collections.Generic;
using System.Linq;

namespace Book_EntityFramework.Repository
{
    interface IPlaceOfWorkRepository
    {
        void Add(PlaceOfWork placeOfWork);
        List<PlaceOfWork> GetAll();
        PlaceOfWork GetById(int id);
        void Update(PlaceOfWork placeOfWork);
        void Delete(int id);
    }
    class PlaceOfWorkRepository : IPlaceOfWorkRepository
    {
        public void Add(PlaceOfWork place)
        {
            using (var dbContext = new FirstDbContext())
            {
                dbContext.PlacesOfWork.Add(place);
                dbContext.SaveChanges();
            }
        }

        public void Delete(int id)
        {
            using (var dbContext = new FirstDbContext())
            {
                var place = dbContext.PlacesOfWork.First(x => x.Id == id);
                dbContext.PlacesOfWork.Remove(place);
                dbContext.SaveChanges();
            }
        }

        public List<PlaceOfWork> GetAll()
        {
            using (var dbContext = new FirstDbContext())
            {
                var place = dbContext.PlacesOfWork.ToList();
                return place;
            }
        }

        public PlaceOfWork GetById(int id)
        {
            using (var dbContext = new FirstDbContext())
            {
                var place = dbContext.PlacesOfWork.First(x => x.Id == id);
                return place;
            }
        }

        public void Update(PlaceOfWork placeOfWork)
        {
            using (var dbContext = new FirstDbContext())
            {
                var oldPlaceOfWork = dbContext.PlacesOfWork.First(x => x.Id == placeOfWork.Id);
                if (oldPlaceOfWork != null)
                {
                    oldPlaceOfWork.Position = placeOfWork.Position;
                    oldPlaceOfWork.AddressOfWork = placeOfWork.AddressOfWork;
                    oldPlaceOfWork.UserId = placeOfWork.UserId;
                }
                dbContext.SaveChanges();
            }
        }
    }
}

﻿using Book_EntityFramework.Model;
using Book_EntityFramework.MyDbContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Book_EntityFramework.Repository
{
    interface IPassportRepository
    {
        void Add(Passport passport);
        Passport GetById(int id);
        void Delete(int id);
        List<Passport> GetAll();
        void Update(Passport passport);
    }
    class PassportRepository : IPassportRepository
    {
        public void Add(Passport passport)
        {
            using (var dbContext = new FirstDbContext())
            {
                dbContext.Passports.Add(passport);
                dbContext.SaveChanges();
            }
        }

        public void Delete(int id)
        {
            using(var dbContext = new FirstDbContext())
            {
                var passport = dbContext.Passports.First(x => x.Id == id);
                dbContext.Passports.Remove(passport);
                dbContext.SaveChanges();
            }
        }

        public List<Passport> GetAll()
        {
            using (var dbContext = new FirstDbContext())
            {
                var passport = dbContext.Passports.ToList();
                return passport;
            }
        }

        public Passport GetById(int id)
        {
            using (var dbContext = new FirstDbContext())
            {
                var password = dbContext.Passports.First(x => x.Id == id);
                return password;
            }
        }

        public void Update(Passport passport)
        {
            using (var dbContext = new FirstDbContext())
            {
                var oldPass = dbContext.Passports.First(x => x.Id == passport.Id);
                if (oldPass != null)
                {
                    oldPass.PassportId = passport.PassportId;
                    oldPass.StartDate = passport.StartDate;
                    oldPass.ExpiredDate = passport.ExpiredDate;
                    oldPass.UserId = passport.UserId;
                }
                dbContext.SaveChanges();
            }
        }
    }
}

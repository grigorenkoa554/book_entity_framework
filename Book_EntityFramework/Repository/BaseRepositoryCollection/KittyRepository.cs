﻿using Book_EntityFramework.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Book_EntityFramework.Repository.BaseRepositoryCollection
{
    class KittyRepository : BaseRepository<Kitty>
    {
        public Kitty GetByIDDeep(int id, KittyJoinType joinTypeEnum)
        {
            return GetQueryJoin(joinTypeEnum).First(x => x.Id == id);
        }

        public List<Kitty> GetAllDeep(KittyJoinType joinTypeEnum)
        {
            return GetQueryJoin(joinTypeEnum).ToList();
        }

        protected override void TraLaLa()
        {
            Console.WriteLine("Tyn tyn tyn!");
            base.TraLaLa();
        }

        private IQueryable<Kitty> GetQueryJoin(KittyJoinType joinTypeEnum)
        {
            var query = dbContext.Kittens.Select(x => x);
            if ((joinTypeEnum & KittyJoinType.Student) == KittyJoinType.Student)
            {
                query = query.Include(x => x.Student);
            }
            if ((joinTypeEnum & KittyJoinType.Course) == KittyJoinType.Course)
            {
                query = query.Include(x => x.Student).ThenInclude(x => x.Course);
            }
            if ((joinTypeEnum & KittyJoinType.Chicken) == KittyJoinType.Chicken)
            {
                query = query.Include(x => x.Student).ThenInclude(x => x.Chicken);
            }
            return query;
        }
    }
    public enum KittyJoinType
    {
        None = 1,
        Student = 2,
        Course = 4,
        Chicken = 8
    }
}

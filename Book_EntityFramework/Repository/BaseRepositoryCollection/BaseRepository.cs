﻿using Book_EntityFramework.Model;
using Book_EntityFramework.MyDbContext;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Book_EntityFramework.Repository.BaseRepositoryCollection
{
    interface IRepository<T>
    {
        void Add(T item);
        void AddRange(List<T> items);
        List<T> GetAll();
        T GetById(int id);
        void Update(T item);
        void Delete(int id);
    }

    public class BaseRepository<T> : IDisposable, IRepository<T> 
        where T : BaseEntity, new() 
    {
        protected readonly FirstDbContext dbContext = new FirstDbContext();
        protected readonly DbSet<T> table;
        public BaseRepository()
        {
            table = dbContext.Set<T>();
        }

        public void Add(T item)
        {
            table.Add(item);
            dbContext.SaveChanges();
        }

        public void AddRange(List<T> items)
        {
            table.AddRange(items);
            dbContext.SaveChanges();
        }

        public void Delete(int id)
        {
            table.Remove(new T { Id = id });
            dbContext.SaveChanges();
        }

        public virtual List<T> GetAll()
        {
            TraLaLa();
            return table.ToList();
        }

        protected virtual void TraLaLa()
        {
            Console.WriteLine("Try ty ty!");
        }

        public virtual T GetById(int id)
        {
            return table.First(x => x.Id == id);
        }

        public virtual void Update(T item)
        {
            table.Update(item);
            dbContext.SaveChanges();
        }

        public void Dispose()
        {
            dbContext?.Dispose();
        }
    }
}

﻿using Book_EntityFramework.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Book_EntityFramework.Repository.BaseRepositoryCollection
{
    public class StudentRepository : BaseRepository<Student>
    {
        public List<Student> GetAllDeep(StudentJoinType studentJoinType)
        {
            return GetQueryForJoin(studentJoinType).ToList();
        }

        public Student GetAllDeep(int id, StudentJoinType studentJoinType)
        {
            return GetQueryForJoin(studentJoinType).First(x => x.Id == id);
        }

        private IQueryable<Student> GetQueryForJoin(StudentJoinType joinType)
        {
            var query = dbContext.Students.Select(x => x);
            if ((joinType & StudentJoinType.Course) == StudentJoinType.Course)
            {
                query = query.Include(x => x.Course);
            }
            return query;
        }
    }

    public enum StudentJoinType
    {
        None = 1,
        Course = 2
    }
}

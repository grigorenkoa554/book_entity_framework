﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using Book_EntityFramework.Model;
using Book_EntityFramework.MyDbContext;
using Book_EntityFramework.Repository;
using Book_EntityFramework.Repository.BaseRepositoryCollection;
using Microsoft.EntityFrameworkCore;

namespace Book_EntityFramework
{
    class Program
    {
        static void Main(string[] args)
        {
            ExampleBaseRepositoryForChicken();
            return;
            ExampleBaseRepositoryStudentRepository();
            ExampleExecuteQueryFromRawString();
            ExampleManyCarsToManyGasStations();
            ExampleManyDriverToManyMedicalClinics();
            ExampleOneDriverToManyMedicalExaminatons();
            ExampleOneCarToManyRepairIvents();
            MyCarManyToManyMehanic();
            ExampleCarManyToManyMehanic();
            ExampleCarOneToManyRacing();
            ExampleDeleteDeepCarDriverInspection();
            ExampleDeepCarDriverInspection();
            ExampleDbContextForPassengerAndInternationalPassport();
            ExampleDbContextForCarAndDriver();
            ExampleDbContextForUser();
            ExampleDbContextForAddress();
            ExampleDbContextForPlaceOfWork();
        }

        public static void ExampleBaseRepositoryForChicken()
        {
            using (var baseRep = new BaseRepository<Chicken>())
            {
                var chickens = new List<Chicken>();
                var myChicken1 = new Chicken
                {
                    Name = "Pippi",
                    Color = "Yellow",
                    Student = new Student
                    {
                        FirstName = "Angelia",
                        LastName = "Popova",
                        Login = "LALALA1",
                        Course = new Course 
                        { 
                            Title = "Differential equations"
                        }
                    }
                };
                var student1 = new Student 
                { 
                    FirstName = "Alexey", 
                    LastName = "Mironenko",
                    Login = "LOLOLO1",
                    Course = new Course
                    {
                        Title = "Differential equations"
                    }
                };
                var myChicken2 = new Chicken 
                { 
                    Name = "Rorri", 
                    Color = "Green",
                    Student  = student1
                };
                chickens.Add(myChicken1);
                chickens.Add(myChicken2);
                baseRep.AddRange(chickens);
                foreach (var item in chickens)
                {
                    Console.WriteLine($@"Id: {item.Id}, Name: {item.Name}, Color: {item.Color}, Student: {item.Student.FirstName} {item.Student.LastName}");
                }
            }
        }

        public static void ExampleBaseRepositoryStudentRepository()
        {
            var course = new Course { Title = "English C1" };
            using (var baseRepository = new BaseRepository<Course>())
            {
                baseRepository.Add(course);
            }
            var student = new Student { FirstName = "Makar", LastName = "Makarovsky", Course = course };
            using (var studentRepository = new StudentRepository())
            {
                studentRepository.Add(student);
            }

        }

        public static void ExampleExecuteQueryFromRawString()
        {
            var maxId = 13;
            var rep = new CarRepository();
            var cars = rep.ExecuteQuery($"SELECT Top 3 * FROM dbo.Car where Id > {maxId}");
            foreach (var item in cars)
            {
                Console.WriteLine($"Id: {item.Id}, Model: {item.Model}");
            }
        }

        public static void ExampleManyCarsToManyGasStations()
        {
            var carRep = new CarRepository();
            var gasStation = new GasStation
            {
                ColomnNubmer = 2,
                TypeOfFuel = "Petrol",
                Date = new DateTime(2020, 4, 16)
            };
            var car1 = new Car
            {
                Model = "Audi",
                Number = "HY456789",
                Driver = new Driver
                {
                    FirstName = "Katerina",
                    LastName = "Lugovaya",
                    License = new License { Text = "Some text" }
                },
                RepairEvents = new List<RepairEvent>
                {
                    new RepairEvent { Date = new DateTime(2020, 7, 23) },
                    new RepairEvent { Date = DateTime.Now }
                },
                CarGasStations = new List<CarGasStation>
                {
                    new CarGasStation
                    {
                        GasStation = new GasStation
                        {
                            ColomnNubmer = 3,
                            Date = new DateTime(2020,3,12),
                            TypeOfFuel = "Petrol"
                        }
                    },
                    new CarGasStation
                    {
                        GasStation = new GasStation
                        {
                            ColomnNubmer = 3,
                            Date = new DateTime(2020,3,12),
                            TypeOfFuel = "Petrol"
                        }
                    },
                    new CarGasStation
                    {
                        GasStation = gasStation
                    }
                }
            };
            var car2 = new Car
            {
                Model = "Nisan",
                Number = "GS6543213",
                Driver = new Driver
                {
                    FirstName = "Leonid",
                    LastName = "Korolev",
                    License = new License { Text = "All fine" }
                },
                RepairEvents = new List<RepairEvent>
                {
                    new RepairEvent { Date = new DateTime(2019, 5, 2) },
                    new RepairEvent { Date = new DateTime(2020, 1, 28) },
                    new RepairEvent { Date = DateTime.Now }
                },
                CarGasStations = new List<CarGasStation>
                {
                    new CarGasStation
                    {
                        GasStation = new GasStation
                        {
                            ColomnNubmer = 3,
                            Date = new DateTime(2020,6,11),
                            TypeOfFuel = "Petrol"
                        }
                    },
                    new CarGasStation
                    {
                        GasStation = new GasStation
                        {
                            ColomnNubmer = 4,
                            Date = new DateTime(2019,1,1),
                            TypeOfFuel = "Petrol"
                        }
                    },
                    new CarGasStation
                    {
                        GasStation = gasStation
                    }
                }
            };
            var list = new List<Car>();
            list.Add(car1);
            list.Add(car2);
            carRep.AddRange(list);
            var car11 = carRep.GetById(car1.Id, CarDeepGet.GasStation);
            var car22 = carRep.GetById(car2.Id, CarDeepGet.GasStation);
            foreach (var item in car11.CarGasStations)
            {
                Console.WriteLine($@"Model: {item.Car.Model}, Number: {item.Car.Number}, TypeOfFuel: {item.GasStation.TypeOfFuel}, NumberOfColomn: {item.GasStation.ColomnNubmer}");
            }
            Console.WriteLine("----------------0--0------------------");
            Console.WriteLine("----------------____------------------");
            foreach (var item in car22.CarGasStations)
            {
                Console.WriteLine($@"Model: {item.Car.Model}, Number: {item.Car.Number}, TypeOfFuel: {item.GasStation.TypeOfFuel}, NumberOfColomn: {item.GasStation.ColomnNubmer}");
            }
        }


        public static void ExampleManyDriverToManyMedicalClinics()
        {
            var carRep = new CarRepository();
            var myMedicalClinic = new MedicalClinic
            {
                Address = "Sotskaya 54",
                PhoneNumber = "+335776345"
            };
            var car1 = new Car
            {
                Model = "Lada Kalina",
                Number = "OK3456",
                Racings = new List<Racing>
                {
                    new Racing { Title = "LegendaryRacing" }
                },
                RepairEvents = new List<RepairEvent>
                {
                    new RepairEvent { Date = DateTime.Now },
                    new RepairEvent { Date = DateTime.Now }
                },
                Driver = new Driver
                {
                    FirstName = "Alxander",
                    LastName = "Ljbovoy",
                    DriverMedicalClinics = new List<DriverMedicalClinic>
                    {
                        new DriverMedicalClinic
                        {
                            MedicalClinic = myMedicalClinic
                        },
                        new DriverMedicalClinic
                        {
                            MedicalClinic = new MedicalClinic
                            {
                                Address = "Sotskaya 54",
                                PhoneNumber = "+335776345"
                            }
                        },
                        new DriverMedicalClinic
                        {
                            MedicalClinic = new MedicalClinic
                            {
                                Address = "Cvetochnaya 20",
                                PhoneNumber = "+294563231"
                            }
                        }
                    }
                }
            };
            var car2 = new Car
            {
                Model = "Mazda 3",
                Number = "HY342452",
                Racings = new List<Racing>
                {
                    new Racing { Title = "ProblemRacing" }
                },
                RepairEvents = new List<RepairEvent>
                {
                    new RepairEvent { Date = DateTime.Now },
                },
                Driver = new Driver
                {
                    FirstName = "Toma",
                    LastName = "Osipenko",
                    DriverMedicalClinics = new List<DriverMedicalClinic>
                    {
                        new DriverMedicalClinic
                        {
                            MedicalClinic = new MedicalClinic
                            {
                                Address = "Sotskaya 54",
                                PhoneNumber = "+335776345"
                            }
                        },
                        new DriverMedicalClinic
                        {
                            MedicalClinic = myMedicalClinic
                        },
                        new DriverMedicalClinic
                        {
                            MedicalClinic = new MedicalClinic
                            {
                                Address = "Koshetkova 8",
                                PhoneNumber = "++445887271"
                            }
                        }
                    }
                }
            };
            var cars = new List<Car>();
            cars.Add(car1);
            cars.Add(car2);
            carRep.AddRange(cars);
            var car11 = carRep.GetById(car1.Id, CarDeepGet.MedicalClinic | CarDeepGet.License);
            foreach (var item in car11.Driver.DriverMedicalClinics)
            {
                Console.WriteLine($@"CarModel = {item.Driver.Car.Model}, LastNameDriver = {item.Driver.LastName},
                     MedicalClinic = {item.MedicalClinic.Address}");
            }
            var car22 = carRep.GetById(car2.Id, CarDeepGet.MedicalClinic);
            foreach (var item in car22.Driver.DriverMedicalClinics)
            {
                Console.WriteLine($@"CarModel = {item.Driver.Car.Model}, LastNameDriver = {item.Driver.LastName},
                     MedicalClinic = {item.MedicalClinic.Address}");
            }
        }

        public static void ExampleOneDriverToManyMedicalExaminatons()
        {
            var carRepository = new CarRepository();
            var car1 = new Car
            {
                Model = "BMB CX5",
                Number = "HG56313",
                Driver = new Driver
                {
                    FirstName = "Antonina",
                    LastName = "Upornaya",
                    MedicalExaminations = new List<MedicalExamination>
                    {
                        new MedicalExamination{ Date = new DateTime(2018,6,12), LastNameOFDoctor = "Smelchakov" },
                        new MedicalExamination{ Date = new DateTime(2018,10,12), LastNameOFDoctor = "Smelchakov" },
                        new MedicalExamination{ Date = DateTime.Now, LastNameOFDoctor = "Konovalova" }
                    }
                },
                Racings = new List<Racing>
                {
                    new Racing { Title = "ChampionRacing" },
                    new Racing { Title = "RealMen" }
                },
                RepairEvents = new List<RepairEvent>
                {
                    new RepairEvent { Date = DateTime.Now },
                    new RepairEvent { Date = DateTime.Now },
                    new RepairEvent { Date = DateTime.Now }
                }
            };
            carRepository.Add(car1);
            var tempCar1 = carRepository.GetById(car1.Id, CarDeepGet.MedicalExamination);
            foreach (var item in tempCar1.Driver.MedicalExaminations) //у car есть driver, у которого есть лист MedicalExaminations
            {
                Console.WriteLine($@"CarId = {item.Driver.CarId}, LastNameDriver = {item.Driver.LastName}, 
                    LastNameOfDoctor = {item.LastNameOFDoctor}, DataMedicalExamiation = {item.Date}");
            }
        }

        public static void ExampleOneCarToManyRepairIvents()
        {
            var carRepository = new CarRepository();
            var car1 = new Car
            {
                Model = "Lada Vesta",
                Number = "ER23573",
                Driver = new Driver { FirstName = "Artem", LastName = "Fartoviy" },
                Racings = new List<Racing>
                {
                    new Racing { Title = "RallyDoccat" },
                    new Racing { Title = "HardRacing" }
                },
                RepairEvents = new List<RepairEvent>
                {
                    new RepairEvent { Date = new DateTime(1999,4,6) },
                    new RepairEvent { Date = DateTime.Now }
                }
            };
            carRepository.Add(car1);
            var car1_1 = carRepository.GetById(car1.Id, CarDeepGet.RepairEvent);
            foreach (var item in car1_1.RepairEvents)
            {
                Console.WriteLine($@"CarId: {item.CarId}, CarModel: {item.Car.Model}, DateEvent: {item.Date}");
            }
        }

        public static void MyCarManyToManyMehanic()
        {
            Console.WriteLine("------MyCarManyToManyMehanic------");
            var carRep = new CarRepository();
            var mehanic = new Mehanic { FirstName = "Vasiliy", LastName = "Koropatin" };
            var car1 = new Car
            {
                Model = "Kia",
                Number = "HJ97356",
                Driver = new Driver
                {
                    FirstName = "Boris",
                    LastName = "Kukushkin",
                    License = new License { Text = "KJ784258" }
                },
                CarMehanics = new List<CarMehanic>
                {
                    new CarMehanic
                    {
                        Mehanic = new Mehanic { FirstName = "Kirill", LastName = "Goroshek" }
                    },
                    new CarMehanic
                    {
                        Mehanic = new Mehanic { FirstName = "Leo", LastName = "diCario" }
                    },
                    new CarMehanic
                    {
                        Mehanic = mehanic
                    }
                }
            };
            var car2 = new Car
            {
                Model = "Tesla",
                Number = "TT876543",
                Driver = new Driver
                {
                    FirstName = "Konstantin",
                    LastName = "Lapushkin",
                    License = new License { Text = "YH1234" }
                },
                CarMehanics = new List<CarMehanic>
                {
                    new CarMehanic
                    {
                        Mehanic = new Mehanic { FirstName = "Dmitriy", LastName = "Kostukevich" }
                    },
                    new CarMehanic
                    {
                        Mehanic = mehanic
                    }
                }
            };
            var listForCars = new List<Car> { };
            listForCars.Add(car1);
            listForCars.Add(car2);
            carRep.AddRange(listForCars);
            //
            var car11 = carRep.GetById(car1.Id, CarDeepGet.Mehanic);
            foreach (var item in car11.CarMehanics)
            {
                Console.WriteLine($"Car1: {car11.Id}, FirstName = {item.Mehanic.FirstName}, LastName = {item.Mehanic.LastName}");
            }
            var car22 = carRep.GetById(car2.Id, CarDeepGet.Mehanic);
            foreach (var item in car22.CarMehanics)
            {
                Console.WriteLine($"Car2: {car22.Id}, FirstName = {item.Mehanic.FirstName}, LastName = {item.Mehanic.LastName}");
            }


            Console.WriteLine("----------------------------------");
        }
        public static void ExampleCarManyToManyMehanic()
        {
            var racing = new Racing { Title = "First_1" };

            var carRep = new CarRepository();
            var car1 = new Car
            {
                Model = "Audi",
                Number = "AE4653",
                Driver = new Driver
                {
                    FirstName = "Neutan",
                    LastName = "Kleuton",
                    License = new License { Text = "TY76842" }
                },
                Racings = new List<Racing> { racing, new Racing { Title = "Second_2" }, new Racing { Title = "Three_3" } },
                CarMehanics = new List<CarMehanic> { new CarMehanic { Mehanic = new Mehanic { FirstName = "Ola", LastName = "Molodec" } } }
            };
            carRep.Add(car1);
            var car2 = carRep.GetById(car1.Id, CarDeepGet.Racing | CarDeepGet.Mehanic);
            foreach (var item in car2.Racings)
            {
                Console.WriteLine($"RacingId = {item.Id}, title = {item.Title}");
            }
            foreach (var item in car2.CarMehanics)
            {
                Console.WriteLine($"Mehanic.Id = {item.Mehanic.Id}, Name = {item.Mehanic.FirstName} {item.Mehanic.LastName}");
            }
        }

        public static void ExampleCarOneToManyRacing()
        {
            var racing = new Racing { Title = "First_1" };

            var carRep = new CarRepository();
            var car1 = new Car
            {
                Model = "Audi",
                Number = "AE4653",
                Driver = new Driver
                {
                    FirstName = "Neutan",
                    LastName = "Kleuton",
                    License = new License { Text = "TY76842" }
                },
                Racings = new List<Racing> { racing, new Racing { Title = "Second_2" }, new Racing { Title = "Three_3" } }
            };
            carRep.Add(car1);
            var car2 = carRep.GetById(car1.Id, CarDeepGet.Racing);
            foreach (var item in car2.Racings)
            {
                Console.WriteLine($"RacingId = {item.Id}, title = {item.Title}");
            }
        }

        public static void ExampleDeleteDeepCarDriverInspection()
        {
            var carRep = new CarRepository();
            var car1 = new Car
            {
                Model = "Audi",
                Number = "AE4653",
                Driver = new Driver
                {
                    FirstName = "Neutan",
                    LastName = "Kleuton",
                    License = new License { Text = "TY76842" }
                }
            };
            carRep.Add(car1);
            Console.WriteLine($@"IdCar = {car1.Id}, IdDriver = {car1.Driver.Id}, IdLicence = {car1.Driver.License.Id}");
            carRep.Delete(car1.Id);
        }

        public static void ExampleDeepCarDriverInspection()
        {
            var carRepository = new CarRepository();
            var car = new Car
            {
                Model = "Mazda CX5",
                Number = "DD3454VC",
                Driver = new Driver
                {
                    FirstName = "Lexa",
                    LastName = "Antonenko",
                    License = new License { Text = "CO345543" }
                },
                Inspection = new Inspection { Data = "12/05/2020" }
            };
            carRepository.Add(car);
            //---
            //var cars = carRepository.GetAll();
            var carDeepGet = CarDeepGet.Driver | CarDeepGet.Inspection | CarDeepGet.License;
            var cars = carRepository.GetAll(carDeepGet);
            foreach (var item in cars)
            {
                Console.WriteLine($@"Model: {item.Model}, FirstName: {item.Driver.FirstName}, 
                    Date: {item.Inspection?.Data}, License: {item.Driver?.License?.Text}");
            }
            Console.WriteLine($@"________________________");
            //
            var carsForGetById = carRepository.GetById(10, carDeepGet);
            Console.WriteLine($@"Model: {carsForGetById.Model}, FirstName: {carsForGetById.Driver.FirstName}, 
                    Date: {carsForGetById.Inspection?.Data}, License: {carsForGetById.Driver?.License?.Text}");
            //
            Console.WriteLine($@"________________________");
            //////
            var car2 = cars.First();
            var driver2 = car2.Driver;
            driver2.FirstName = "NewCola";
            carRepository.UpdateDriver(driver2);

            var car3 = cars.Skip(1).First();
            car3.Driver.FirstName = "Second cola";
            carRepository.Update(car3);
            //---
            //var cars = carRepository.GetAll();
            var carDeepGet2 = CarDeepGet.Driver | CarDeepGet.Inspection | CarDeepGet.License;
            var cars2 = carRepository.GetAll(carDeepGet2);
            foreach (var item in cars2)
            {
                Console.WriteLine($@"Model: {item.Model}, FirstName: {item.Driver.FirstName}, 
                    Date: {item.Inspection?.Data}, License: {item.Driver?.License?.Text}");
            }
            //////
        }

        public static void ExampleDbContextForCarAndDriver()
        {
            using (var dbContext = new FirstDbContext())
            {
                var driver = new Driver { FirstName = "Misha", LastName = "Zaharenko" };
                var inspection = new Inspection { Data = "12/06/2019" };
                var car = new Car { Model = "Mazda", Number = "AA3344BB", Driver = driver, Inspection = inspection };
                dbContext.Cars.Add(car);
                dbContext.SaveChanges();

            }
            using (var dbContext = new FirstDbContext())
            {
                var car = dbContext.Cars.Include(c => c.Driver).First();
                var str = $"Car model: {car.Model}, car number: {car.Number}, driver: {car.Driver.FirstName}-{car.Driver.LastName}";
                Console.WriteLine(str);
                var car2 = dbContext.Cars.Include(x => x.Inspection).First();
                //var car2 = dbContext.Cars.First();
                Console.WriteLine(car2.Model);
            }
            using (var dbContext = new FirstDbContext())
            {
                var car2 = dbContext.Cars.First();
                Console.WriteLine(car2.Model);
            }
            using (var dbContext = new FirstDbContext())
            {
                var count = dbContext.Cars.Count();
                Console.WriteLine(count);
            }
            using (var dbContext = new FirstDbContext())
            {
                var car = dbContext.Cars
                    .Include(c => c.Driver)
                    .Where(x => x.Id == 2 && x.Driver.FirstName == "Misha")
                    .First();
                Console.WriteLine(car.Model);
            }
        }
        public static void ExampleDbContextForPassengerAndInternationalPassport()
        {
            using (var dbContext = new FirstDbContext())
            {
                var passenger = new Passenger { FirstName = "Lilia", LastName = "Alilueva" };
                var interPassport = new InternationalPassport { StartDate = "19/03/2019", ExpiredDate = "19/03/2023", Passenger = passenger };
                var passenger1 = new Passenger { FirstName = "Piret", LastName = "Koshak" };
                var interPassport1 = new InternationalPassport { StartDate = "06/02/2018", ExpiredDate = "06/02/2022", Passenger = passenger1 };
                dbContext.InternationalPassports.Add(interPassport);
                dbContext.InternationalPassports.Add(interPassport1);
                dbContext.SaveChanges();
                Console.WriteLine($"We add InternationalPassports for Passengers");
            }
            using (var dbContext = new FirstDbContext())
            {
                var interPassport = dbContext.InternationalPassports.Include(x => x.Passenger).First();
                var data = $"StartDate: {interPassport.StartDate}, ExpiredDate : {interPassport.ExpiredDate}, passenger: {interPassport.Passenger.FirstName}-{interPassport.Passenger.LastName}";
                Console.WriteLine(data);
                var data2 = dbContext.InternationalPassports.First();
                Console.WriteLine(data2.StartDate);
            }
            using (var dbContext = new FirstDbContext())
            {
                var data2 = dbContext.InternationalPassports.First();
                Console.WriteLine(data2.StartDate);
            }
            using (var dbContext = new FirstDbContext())
            {
                var countPass = dbContext.Passengers.Count();
                Console.WriteLine(countPass);
            }
            using (var dbContext = new FirstDbContext())
            {
                var interPassport = dbContext.InternationalPassports
                    .Include(x => x.Passenger)
                    .Where(x => x.Id == 2 && x.Passenger.FirstName == "Piter")
                    .First();
                Console.WriteLine(interPassport.StartDate);
            }
        }

        public static void ExampleDbContextForPassport()
        {
            using (var dbContext = new FirstDbContext())
            {
                dbContext.Passports.Add(new Passport
                {
                    StartDate = "12/02/2018",
                    ExpiredDate = "12/02/2022",
                    PassportId = "HB2304934",
                    UserId = 1
                });
                dbContext.Passports.Add(new Passport
                {
                    StartDate = "31/05/2020",
                    ExpiredDate = "31/05/2024",
                    PassportId = "HB2364751",
                    UserId = 2
                });
                dbContext.SaveChanges();
                Console.WriteLine($"We add Password for User(s)");
            }
        }

        public static void ExampleDbContextForPlaceOfWork()
        {
            using (var dbContext = new FirstDbContext())
            {
                dbContext.PlacesOfWork.Add(new PlaceOfWork
                {
                    Position = "manager",
                    AddressOfWork = "WhiteRiver 87",
                    UserId = 1
                });
                dbContext.PlacesOfWork.Add(new PlaceOfWork
                {
                    Position = "teacher",
                    AddressOfWork = "GoodStreet 12",
                    UserId = 2
                });
                dbContext.SaveChanges();
                Console.WriteLine($"We add PlacesOfWork for User(s)");
            }
        }

        public static void ExampleDbContextForUser()
        {
            using (var dbContext = new FirstDbContext())
            {
                var user = new User
                {
                    FirstName = "Ivan",
                    LastName = "Petrov",
                    Age = 34
                };
                dbContext.Users.Add(user); // добавляем пользователя в локальную коллекцию Users
                dbContext.Users.Add(user); // добавляем пользователя в локальную коллекцию Users
                dbContext.SaveChanges(); // происходит запись с локальной коллекции Users в таблицу User
                Console.WriteLine($"Id of user: {user.Id}");
            }
        }

        public static void ExampleDbContextForAddress()
        {
            using (var dbContext = new FirstDbContext())
            {
                var address = new Address
                {
                    City = "Vologda",
                    Street = "Solnechnaya 7/14",
                    UserId = 2,
                    Description = "Nice place."
                };
                dbContext.Addresses.Add(address);
                dbContext.SaveChanges();
                Console.WriteLine($"Id of address: {address.Id}");
            }
        }
    }
}

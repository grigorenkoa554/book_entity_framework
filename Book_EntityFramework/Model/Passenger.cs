﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Book_EntityFramework.Model
{
    [Table("Passenger")]
    public class Passenger
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public InternationalPassport InternationalPassport { get; set; }
    }

    [Table("InternationalPassport")]
    public class InternationalPassport
    {
        public int Id { get; set; }
        public string StartDate { get; set; }
        public string ExpiredDate { get; set; }

        public Passenger Passenger { get; set; }
        public int PassengerId { get; set; }
    }
}

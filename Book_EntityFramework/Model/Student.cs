﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Book_EntityFramework.Model
{
    public class BaseEntity
    {
        public int Id { get; set; }
    }

    [Table("Course")]
    public class Course : BaseEntity
    {
        public string Title { get; set; }

        public List<Student> Student { get; set; }
    }

    [Table("Student")]
    public class Student : BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Login { get; set; }

        public int CourseId { get; set; }
        public Course Course { get; set; }

        public Chicken Chicken { get; set; }
        public Kitty Kitty { get; set; }
    }

    [Table("Chicken")]
    public class Chicken : BaseEntity
    {
        public string Name { get; set; }
        public string Color { get; set; }

        public int StudentId { get; set; }
        public Student Student { get; set; }
    }

    [Table("Kitty")]
    public class Kitty : BaseEntity
    {
        public string Тickname { get; set; }
        public string Color { get; set; }

        public Student Student { get; set; }
        public int StudentId { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Book_EntityFramework.Model
{
    [Table("PlaceOfWork")]
    public class PlaceOfWork
    {
        [Key]
        public int Id { get; set; }
        public string Position { get; set; }
        public string AddressOfWork { get; set; }
        [ForeignKey("UserId")]
        public int UserId { get; set; }
    }
}

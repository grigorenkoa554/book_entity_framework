﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Book_EntityFramework.Model
{
    [Table("Address")] 
    public class Address
    {
        [Key]
        public int Id { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        //[NotMapped]
        public string Description { get; set; }
        public string PhoneNumber { get; set; }

        [ForeignKey("UserId")]
        public int UserId { get; set; }
    }
}

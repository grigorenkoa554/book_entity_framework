﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Book_EntityFramework.Model
{
    [Table("Passport")]
    public class Passport
    {
        public int Id { get; set; }
        public string StartDate { get; set; }
        public string ExpiredDate { get; set; }
        public string PassportId { get; set; }
        [ForeignKey("UserId")]
        public int UserId { get; set; }
    }
}

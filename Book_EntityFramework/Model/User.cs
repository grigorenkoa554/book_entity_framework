﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Book_EntityFramework.Model
{
    [Table("User")] //таблица в БД будет называться User
    public class User //создаём класс User, на базе которого будет сгенерирована таблица User 
    {
        //[Key]
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
    }
}

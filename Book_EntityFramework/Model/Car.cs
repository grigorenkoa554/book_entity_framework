﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Book_EntityFramework.Model
{
    [Table("Car")]
    public class Car
    {
        [Key]
        public int Id { get; set; }
        public string Model { get; set; }
        public string Number { get; set; }

        public Driver Driver { get; set; }
        public Inspection Inspection { get; set; }
        public List<Racing> Racings { get; set; }
        public List<RepairEvent> RepairEvents { get; set; }

        public List<CarMehanic> CarMehanics { get; set; }

        public List<CarGasStation> CarGasStations { get; set; }
    }

    [Table("CarGasStation")]
    public class CarGasStation
    {
        public int CarId { get; set; }
        public Car Car { get; set; }

        public int GasStationId { get; set; }
        public GasStation GasStation { get; set; }
    }


    [Table("GasStation")]
    public class GasStation
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public string TypeOfFuel { get; set; }
        public int ColomnNubmer { get; set; }

        public List<CarGasStation> CarGasStations { get; set; }
    }

    [Table("RepairEvent")]
    public class RepairEvent
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public Car Car { get; set; }
        public int CarId { get; set; }

    }

    [Table("CarMehanic")]
    public class CarMehanic
    {
        public int CarId { get; set; }
        public Car Car { get; set; }

        public int MehanicId { get; set; }
        public Mehanic Mehanic { get; set; }
    }

    [Table("Mehanic")]
    public class Mehanic
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public List<CarMehanic> CarMehanics { get; set; }
    }

    [Table("Racing")]
    public class Racing
    {
        public int Id { get; set; }
        public string Title { get; set; }

        public Car Car { get; set; }
        public int CarId { get; set; }
    }

    [Table("Driver")]
    public class Driver
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        //public int UserId { get; set; }

        public Car Car { get; set; }
        public int CarId { get; set; }

        public License License { get; set; }

        public List<MedicalExamination> MedicalExaminations { get; set; }

        public List<DriverMedicalClinic> DriverMedicalClinics { get; set; }
    }

    [Table("DriverMedicalClinic")]
    public class DriverMedicalClinic
    {
        public Driver Driver { get; set; }
        public int DriverId { get; set; }

        public MedicalClinic MedicalClinic { get; set; }
        public int MedicalClinicId { get; set; }
    }

    // TODO: 
    //медицинская клиника - может быть много драйверов
    // many to many
    [Table("MedicalClinic")]
    public class MedicalClinic
    {
        public int Id { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }

        public List<DriverMedicalClinic> DriverMedicalClinics { get; set; }
    }

    //medical examination
    // one to many
    [Table("MedicalExamination")]
    public class MedicalExamination
    {
        public int Id { get; set; }
        public string LastNameOFDoctor { get; set; }
        public DateTime Date { get; set; }

        public Driver Driver { get; set; }
        public int DriverId { get; set; }
    }

    [Table("License")]
    public class License
    {
        public int Id { get; set; }
        public string Text { get; set; }

        public Driver Driver { get; set; }
        public int DriverId { get; set; }
    }

    [Table("Inspection")]
    public class Inspection
    {
        public int Id { get; set; }
        public string Data { get; set; }

        public Car Car { get; set; }
        public int CarId { get; set; }
    }
}

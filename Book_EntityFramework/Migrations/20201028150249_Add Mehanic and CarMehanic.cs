﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Book_EntityFramework.Migrations
{
    public partial class AddMehanicandCarMehanic : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Mehanic",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Mehanic", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CarMehanic",
                columns: table => new
                {
                    CarId = table.Column<int>(nullable: false),
                    MehanicId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CarMehanic", x => new { x.CarId, x.MehanicId });
                    table.ForeignKey(
                        name: "FK_CarMehanic_Car_CarId",
                        column: x => x.CarId,
                        principalTable: "Car",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CarMehanic_Mehanic_MehanicId",
                        column: x => x.MehanicId,
                        principalTable: "Mehanic",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CarMehanic_MehanicId",
                table: "CarMehanic",
                column: "MehanicId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CarMehanic");

            migrationBuilder.DropTable(
                name: "Mehanic");
        }
    }
}

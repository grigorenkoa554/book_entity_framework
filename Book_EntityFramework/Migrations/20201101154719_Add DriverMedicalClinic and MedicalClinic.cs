﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Book_EntityFramework.Migrations
{
    public partial class AddDriverMedicalClinicandMedicalClinic : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "MedicalClinic",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Address = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MedicalClinic", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DriverMedicalClinic",
                columns: table => new
                {
                    DriverId = table.Column<int>(nullable: false),
                    MedicalClinicId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DriverMedicalClinic", x => new { x.DriverId, x.MedicalClinicId });
                    table.ForeignKey(
                        name: "FK_DriverMedicalClinic_Driver_DriverId",
                        column: x => x.DriverId,
                        principalTable: "Driver",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DriverMedicalClinic_MedicalClinic_MedicalClinicId",
                        column: x => x.MedicalClinicId,
                        principalTable: "MedicalClinic",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DriverMedicalClinic_MedicalClinicId",
                table: "DriverMedicalClinic",
                column: "MedicalClinicId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DriverMedicalClinic");

            migrationBuilder.DropTable(
                name: "MedicalClinic");
        }
    }
}
